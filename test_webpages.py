import time
import pytest
from selenium import webdriver

class TestClass:

    def test_one(self):
        print("This is the google webpage test")
        # create the driver object of the Chrome browser
        driver = webdriver.Chrome(executable_path="C:\\PythonCodes\\SeleniumCodes\\Drivers\\chromedriver_win32\\chromedriver.exe")

        # open google.com
        webpage = "https://www.google.com/"
        driver.get(webpage)

        # get the title of the webpage
        print("Title of the webpage :", driver.title)
        driver.save_screenshot("test_one.png")
        # Close the browser
        time.sleep(2)
        print("Closing the browser")
        driver.close()

    def test_two(self):
        print("This is the yahoo webpage test")
        # create the driver object of the Chrome browser
        driver = webdriver.Chrome(executable_path="C:\\PythonCodes\\SeleniumCodes\\Drivers\\chromedriver_win32\\chromedriver.exe")

        # open google.com
        webpage = "https://www.airtel.com/"
        driver.get(webpage)

        # get the title of the webpage
        print("Title of the webpage :", driver.title)
        driver.save_screenshot("test_two.png")
        assert driver.title == 'yahoo', "Title should be yahoo"

        # Close the browser
        time.sleep(2)
        print("Closing the browser")
        driver.close()


    def test_three(self):
        print("This is the microsoft webpage test")
        # create the driver object of the Chrome browser
        driver = webdriver.Chrome(executable_path="C:\\PythonCodes\\SeleniumCodes\\Drivers\\chromedriver_win32\\chromedriver.exe")

        # open google.com
        webpage = "https://www.microsoft.com/"
        driver.get(webpage)

        # get the title of the webpage
        print("Title of the webpage :", driver.title)
        driver.save_screenshot("test_three.png")

        # Close the browser
        time.sleep(2)
        print("Closing the browser")
        driver.close()
